<?php

function pt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'DanaBanana_secret_key';
    $secret_iv = 'DanaBanana_secret_iv';
    
    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $string= "DanaBanana#112" . $string;
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}



function phoneNumberDisplay(){


   $displayPhone="1-888-355-1605";
    if(($_SESSION["fbclid"] !=="") OR  ($_SESSION['utm_source']=="facebook")) {
        $displayPhone="1-855-805-4936";
    }

    
    return $displayPhone;

}
?>