/*****  Header  ****/
$(window).scroll(function() {
    if ($(this).scrollTop() > 100){  
        $('header').addClass("sticky");
    }
    else{
        $('header').removeClass("sticky");
    }
});



/*****  Slider  ****/
var owl = $('.hero-carousel');
      owl.owlCarousel({
        margin:0,
        loop: true,
		dots:true,
		nav:false,
		autoplay:true,
		items:5,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 1
          },
		  768: {
            items: 1
          },
		  992: {
            items: 1
          },
          1000: {
            items:1,
          }
        }
      })
	  
var owl = $('.testimonial-carousel');
      owl.owlCarousel({
        margin:30,
        loop: true,
		dots:false,
		nav:false,
		autoplay:true,
		items:5,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 2
          },
		  768: {
            items: 3
          },
		  992: {
            items: 3
          },
          1000: {
            items:3,
          }
        }
      })
	  
	  
var owl = $('.thumb-carousel');
      owl.owlCarousel({
        margin:0,
        loop: true,
		dots:false,
		nav:false,
		autoplay:true,
		autoplayTimeout:1500,
    	autoplayHoverPause:true,
		items:5,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 2
          },
		  768: {
            items: 3
          },
		  992: {
            items: 4
          },
          1000: {
            items:4
          },
         1280: {
            items:5,
          }
		  
        }
      })	  
	  