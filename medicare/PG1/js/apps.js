//Animate triangles
	
jQuery('').bind('inview', function (event, visible) {
		jQuery(this).addClass("animated fadeIn");
});

jQuery('.prest_feature i,.spe_ftr figure,.choose_block figure').bind('inview', function (event, visible) {
		jQuery(this).addClass("animated zoomIn");
});

jQuery('.fleet_block, .prest_feature').bind('inview', function (event, visible) {
		jQuery(this).addClass("animated fadeInUp");
});

jQuery('').bind('inview', function (event, visible) {
		jQuery(this).addClass("animated fadeInLeft");
});

jQuery('.hero_sec .hero_form').bind('inview', function (event, visible) {
		jQuery(this).addClass("animated fadeInRight");
});
