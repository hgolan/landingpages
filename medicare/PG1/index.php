<?php

session_start();
include_once('../../Include/handlers/page_load.php');
include_once('../../lib/config.php');
include_once('../../lib/functions.php');



$gsx="d034a839-8ba8-40c7-a570-a67f9c43de5b";
$oid="PG1";


$displayPhoneNumber=phoneNumberDisplay();



//protect injection
$auth=pt_decrypt('encrypt', time());


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="theme-color" content="#00539b" />
<link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
<title>Medicare Advisory Service</title>
<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/fonts.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">

<meta name="facebook-domain-verification" content="adwq7werxmm14td9c6pqfgo8eha97k" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N6SR8H7');</script>
<!-- End Google Tag Manager -->


<script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
<script src="js/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>


<meta property="og:url"                content="https://campaigns.amac.us/landingpages/medicare/PG1/index.php" />
<meta property="og:type"               content="website" />
<meta property="og:title"              content="AMAC Medicare Advantage Plan" />
<meta property="og:description"        content="Get the Right Medicare Advantage Plan for You" />
<meta property="og:image"              content="https://campaigns.amac.us/landingpages/medicare/PG1/images/FBPost.png" />
</head>
<body>



    <input type="hidden" id="gsx" name="gsx" value="<?php echo($gsx); ?>">
    <input type="hidden" id="oid" name="oid" value="<?php echo($oid); ?>">
    <input type="hidden" id="utm_source" name="utm_source" value="<?php echo($_SESSION["utm_source"]); ?>">
    <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo($_SESSION["utm_medium"]); ?>" >
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo($_SESSION["utm_campaign"]); ?>">
    <input type="hidden" id="utm_term" name="utm_term" value="<?php echo($_SESSION["utm_term"]); ?>">
    <input type="hidden" id="gclid" name="gclid" value="<?php echo($_SESSION["gclid"]); ?>" >
    <input type="hidden" id="fbclid" name="fbclid" value="<?php echo($_SESSION["fbclid"]); ?>">
    <input type="hidden" id="fbc" name="fbc" value="<?php echo($_SESSION["fbclid"]); ?>">
    <input type="hidden" id="referer" name="referer" value="<?php echo(urlencode($_SESSION["HTTP_REFERER"])); ?>">
    <input type="hidden" id="src" name="src" value="<?php echo($_SESSION["src"]); ?>">
    
    <input type="hidden" id="auth" name="auth" value="<?php echo($auth); ?>">
    <input type="hidden" id="utm_content" name="utm_content" value="<?php echo($_SESSION["utm_content"]); ?>">
    
    
    


    <!--
    <input type="hidden" id="ref" name="ref" value="https://www.sunfirematrix.com/app/consumer/asrn/?page=pg1&name={firstname}&phone={phone}&utm_source={utm_source}&utm_medium={utm_medium}&utm_campaign={utm_campaign}&gclid={gclid}&fbclid={fbclid}&fbc={fbc}"  >
    -->
 
    <input type="hidden" id="ref" name="ref" value="https://sunfirematrix.amac.us/app/consumer/asrn/?utm_source=myamacmedicare#/plans/{zip}/36103/MAPD"  />
    


<!-- Header -->
<header>
<div class="container">
	<div class="row">
    	<div class="col-md-4 col-sm-12">
        	<a href="#" class="logo" title="Medicare Advisory Service"><img src="images/logo.png" alt="Medicare Advisory Service" /></a>
        </div> 
        <div class="col-md-8 col-sm-12">
        	<div class="call">
        	Speak with a licensed insurance agent <a href="tel:<?php echo($displayPhoneNumber);?>"><?php echo($displayPhoneNumber);?></a><strong> Mon. - Fri. 9am - 7pm EST </strong>
            </div>
        </div>
    </div>
</div>
</header>
<!-- Section -->
<section>
<!--Hero-->
<div class="hero_sec">
	<div class="container">
    	<div class="row align-items-center">
        	<div class="col-lg-8 col-md-12 col-sm-12">
            	<div class="hero_form">
                	<h2 class="title">Get the Right Medicare Advantage Plan for You</h2>
                    <p>Choose from a variety of Medicare Advantage and Prescription Drug plans in your area.</p>
                    <div class="form_in">
                    	<div class="row">
                    	<div class="form-group col-md-6">
                    		<input class="form-control" type="text" id="firstname" name="firstname" placeholder="Full Name*">
                    	</div>
                        <div class="form-group col-md-6">
                    		<input class="form-control" type="text" id="zipcode" name="zipcode" placeholder="zipcode*">
                    	</div>
                        <div class="form-group col-md-6">
                    		<input class="form-control" type="text"  id="phone" name="phone" placeholder="Phone Number*">
                    	</div>
                        <div class="form-group col-md-6">
                    		<input class="form-control" type="email"   id="email" name="email" placeholder="Email Address">
                    	</div>
                     
                        <div class="form-group col-md-12">

                        
                        <button type="button" class="btn btn-green" id="cta"   onclick="validateForm()">
 
                             <span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true" style="display:none" ></span>
                        Submit
                        </button>

                    	</div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
            	<figure><img src="images/hero_img.jpg" alt="" /></figure>
            </div>
        </div>
    </div>
</div> 
<!--Sponser Logos-->
<div class="sp_logos">
	<div class="container">
    	<ul>
        	<li><img src="images/sp_logo1.png" alt=""></li>
            <li><img src="images/sp_logo2.png" alt=""></li>
            <li><img src="images/sp_logo3.png" alt=""></li>
            <li><img src="images/sp_logo4.png" alt=""></li>
            <li><img src="images/sp_logo5.png" alt=""></li>
        </ul>
        
    </div>
</div>
<!--why-->
<div class="why">
	<div class="container">
        <center>
    	<h2 class="title">Why AMAC is better for Medicare?</h2>
    </center>
        <div class="row">
        	<div class="col-md-4 col-sm-4">
            	<div class="why_block">
            	<figure><img src="images/ico1.png" alt="" /></figure>
                <h3>Personalized Service</h3>
                <ul>
                	<li>You speak with a dedicated Advisor who becomes your single point of contact each time you call! </li>
                    <li>Our trusted Advisors are a team of  highly trained and licensed professionals  you can count on. </li>
                </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
            	<div class="why_block">
            	<figure><img src="images/ico2.png" alt="" /></figure>
                <h3>More Choices</h3>
                <ul>
                	<li>One size does not fit all! At AMAC, we find the right plan to meet your individual needs. </li>
                </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
            	<div class="why_block">
            	<figure><img src="images/ico3.png" alt="" /></figure>
                <h3>Your Voice in Washington</h3>
                <ul>
                	<li>We are the ONLY conservative senior organization with representation in Washington! </li>
                    <li>We are fighting to protect and strengthen  your Medicare and Social Security.</li>
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Plan-->
<div class="plan">
	<div class="container">
    	<div class="row align-items-center">
        	<div class="col-md-5 col-sm-12">
            	<figure><img src="images/hero_img.jpg" alt="" /></figure>
            </div>
            <div class="col-md-7 col-sm-12">
            	<h2 class="title mb-4">Searching for a Medicare plan that meets your needs?</h2>
              
               <p>Get a quick quote now!  With so many A-rated insurance companies to choose from, AMAC is able to deliver more CHOICES of coverage.</p>
               
            <p>
            Shop online or over the phone.  Either way, you’ll get support from our team of highly trained, licensed professionals.  
            </p>
            </div>
        </div>
    </div>
</div>
<!--FAQ-->
<div class="faq_sec">
	<div class="container">
     <p style="font-size:12px;text-align:left">
     By submitting this form, you are consenting to receive information and/or be contacted by a licensed insurance agent who represents various health insurance companies such as Humana, Aetna, Mutual of Omaha, Coventry, Anthem, and Blue Cross Blue Shield, amongst others. This is not an application for insurance and does not replace any coverage you may currently have. AMAC does not recommend health related products, services, or insurance plans. You are strongly encouraged to evaluate your needs. You are not obligated to provide consent in order to use our services or make a purchase. Not connected with or endorsed by the U.S. Government or federal Medicare program.
     </p>
    </div>
</div>
 

</section>

<!-- Footer -->
<footer>
	
<div id="btn-back-to-top">   <img src="images/contact-us.png"/>  </div>



<div class="container">
	<div class="ftr_logo"><img src="images/ftr_logo.png" alt="" /></div>
    <div class="copy_right">
    © 2021 AMAC Inc.  All Rights Reserved.  <a href="#">Privacy Policy</a>  |  <a href="#">Terms of Service</a>
    </div>
</div>    
</footer>
<!--Modal-->
<div class="modal fade" id="Subscribes" tabindex="-1" role="dialog" aria-labelledby="newsleter" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
          <div style="width:100%">
        <h2 class="title" id="newsleter">Need help finding a plan?</h2>
            </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeMe()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>AMAC can help you find the right Medicare Advantage and Prescription Drug plans!</p>
        <div class="email_form">
        	<div class="row">
            	<div class="form-group col-md-6">
                	<input type="text" class="form-control" id="fname_sec" name="fname_sec" placeholder="First Name">
                </div>
                <div class="form-group col-md-6">
                	<input type="number" class="form-control" id="phone_sec" name="phone_sec" placeholder="Phone Number">
                </div>
                <div class="form-group col-md-12">
                    <input class="btn btn-green"  onclick="validateExitForm()" name="cta" id="cta" value="SUBMIT" >
                 </div>
            </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!-- Js -->

<script type="text/javascript">

function closeMe(){

    $('#Subscribes').modal('hide');
    
}

function validateExitForm(){


            var valid=true;

            $("#fname_sec").removeClass( "is-invalid" );
            $("#fname_sec").removeClass( "is-valid" );

            $("#phone_sec").removeClass( "is-invalid" );
            $("#phone_sec").removeClass( "is-valid" );


            if($("#fname_sec").val()==''){
                valid=false;
                $("#fname_sec").addClass( "is-invalid" );
            }else{
                $("#fname_sec").addClass( "is-valid" );
            }
  

            var phone=$("#phone_sec").val();
            $("#phone_sec").val(phone.replace(/^\+[0-9]/, ''))
          
            
             
            if(phonenumber($("#phone_sec").val())==false){
                valid=false;
                $("#phone_sec").addClass( "is-invalid" );
            }else{
                $("#phone_sec").addClass( "is-valid" );
            }


            if(valid==true){
                submitPage(1);
            }
            
 

}






    function validateForm(){
 
 
        //is-valid
        //is-invalid
        var valid=true;

        $("#firstname").removeClass( "is-invalid" );
        $("#firstname").removeClass( "is-valid" );

        $("#zipcode").removeClass( "is-invalid" );
        $("#zipcode").removeClass( "is-valid" );

        $("#phone").removeClass( "is-invalid" );
        $("#phone").removeClass( "is-valid" );

        $("#email").removeClass( "is-invalid" );
        $("#email").removeClass( "is-valid" );


        




            if($("#firstname").val()==''){
                valid=false;
                $("#firstname").addClass( "is-invalid" );
            }else{
                $("#firstname").addClass( "is-valid" );
            }

           

           if(!isValidUSZip($("#zipcode").val())){
              valid=false;
                $("#zipcode").addClass( "is-invalid" );
           }else{
                $("#zipcode").addClass( "is-valid" );
            }
   
          
            
            var phone=$("#phone").val();
          //  $("#phone").val(phone.replace(/^\+[0-9]/, ''))
          
            
             
            if(phonenumber($("#phone").val())==false){
                valid=false;

                $("#phone").addClass( "is-invalid" );
            }else{
                $("#phone").addClass( "is-valid" );
            }

            if(valid==true){    
                submitPage(0)
            }
            
    }


    
function isValidUSZip(sZip) {
   return /^\d{5}(-\d{4})?$/.test(sZip);
}


function phonenumber(inputtxt)
{
   
   
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

    return re.test(inputtxt);
}

  //  $(window).on('load', function() {
    //  $('#Subscribes').modal('show');
   // });




    var POPPROonexit = (function(){
function detectMobileBrowsers() {var a=navigator.userAgent||navigator.vendor||window.opera;if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))return true;return false;}
var actualMedia = detectMobileBrowsers() ? 'mobile' : 'desktop';

 
 
 



var darkScreen;
var tempY, chackTheMous = 1;
var foo = sessionStorage.getItem("pdb_exit_pu");

 
if (actualMedia=="desktop"){
	if(foo == null){
		setTimeout(function() {startM(); }, 2000);
	}
}


function startM(){
	setInterval(function(){ document.onmousemove = getMouseY;}, 2000);
}
 
 
function getMouseY(e) {
	if (chackTheMous == 1) {
		tempY = e.clientY;
		if (tempY<15) {
            var foox = sessionStorage.getItem("pdb_exit_pu");
            if(foox == null){
                $('#Subscribes').modal('show');
                 sessionStorage.setItem('pdb_exit_pu', '1');

            }
        }
	}
		  //}
}

 
  
function removeElement(element) {
    element && element.parentNode && element.parentNode.removeChild(element);
}


 
 
 

}());




var dat;



function submitPage(flg){

  
     
    var gsx=$("#gsx").val();
    var oid=$("#oid").val();
    var utm_source=$("#utm_source").val();
    var utm_medium=$("#utm_medium").val();
    var utm_campaign=$("#utm_campaign").val();
    var utm_term=$("#utm_term").val();
    var gclid=$("#gclid").val();
    var fbclid=$("#fbclid").val();
    var referer=$("#referer").val();
    var src=$("#src").val();
    var auth=$("#auth").val();

    var firstname="";
    var lastname="";
    var phone="";
    var email="";
    var zipcode="";

    var fbp = document.cookie.split(';').filter(function (c) {
        return c.includes('_fbp=');
      }).map(function (c) {
        return c.split('_fbp=')[1];
      });
      var fbc = document.cookie.split(';').filter(function (c) {
        return c.includes('_fbc=');
      }).map(function (c) {
        return c.split('_fbc=')[1];
      });
      fbp = fbp.length && fbp[0] || '';
      fbc = fbc.length && fbc[0] || '';



    var ref=$("#ref").val();


    if(flg==0){
        firstname=$("#firstname").val();
        lastname=$("#lastname").val();
        phone=$("#phone").val();
        email=$("#email").val();
        zipcode=$("#zipcode").val();
    } else{
        firstname=$("#fname_sec").val();
        phone=$("#phone_sec").val();
    }

     

    $("#spinner").show();

    dataLayer = window.dataLayer || [];
    dataLayer.push({'event': 'lead'});


    var clientData="";
    if (sessionStorage['clientData']) {

        clientData = sessionStorage.getItem("clientData");
    }
       
 

    
    $.post( "<?php echo(API_URL);?>", { action: "create", src: src,fbp: fbp,fbc: fbc, auth: auth,referer: referer, gsx: gsx, oid: oid, utm_source: utm_source,utm_medium: utm_medium,utm_campaign: utm_campaign,utm_term: utm_term,gclid: gclid,fbclid: fbclid,firstname: firstname,lastname: lastname, phone: phone,email: email,ref: ref,clientData: clientData,zip: zipcode})
.done(function( data ) {
     
         dat= JSON.stringify(data);
         window.location.href = data.ref;

});  


}
 
function headerStuff(){
  if ($(window).scrollTop() > 200) {

     $('header').addClass('sticky');
  } 
  else {
 
    $('header').removeClass('sticky');
  }
};

$(document).ready(function() {
  headerStuff();
  $(window).scroll(function(){
    headerStuff();
  });
 
});
 


 
let mybutton = document.getElementById("btn-back-to-top");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 20 ||
    document.documentElement.scrollTop > 20
  ) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
mybutton.addEventListener("click", backToTop);

function backToTop() {
//  document.body.scrollTop = 0;
 // document.documentElement.scrollTop = 0;

 $('html, body').animate({scrollTop:0}, 1000);       


}


 

$( document ).ready(function() {
$.getJSON("https://admin.sitekicks.ai/API/geo.aspx?action=iplocation&apikey=62123EDD-EBFA-4C65-9802-DF719BFDE6F2&callback=?", function (data) {
var JSONObject = JSON.parse(JSON.stringify(data));
sessionStorage.setItem("clientData",JSON.stringify(data))
});
});





</script>

<!--
<script id='skjsws' src='https://sitekicks.s3.eu-west-1.amazonaws.com/ws_banner/aff-08228a73-ef96-4bf0-93f0-af58ca1e6a01.js' > </script>
-->

</body>
</html>